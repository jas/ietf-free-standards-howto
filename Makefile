all: draft-josefsson-free-standards-howto.txt draft-josefsson-free-standards-howto.html

clean:
	rm *~ draft-josefsson-free-standards-howto.txt draft-josefsson-free-standards-howto.html

draft-josefsson-free-standards-howto.txt: draft-josefsson-free-standards-howto.xml
	xml2rfc $<

draft-josefsson-free-standards-howto.html: draft-josefsson-free-standards-howto.xml
	xml2rfc $< $@
